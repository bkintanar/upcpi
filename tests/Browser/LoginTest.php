<?php

/*
 * This file is part of the UPCPI Software package.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the 3-clause BSD License.
 *
 * This source file is subject to the 3-clause BSD License that is
 * bundled with this package in the LICENSE file.
 *
 * @version    alpha
 *
 * @author     Bertrand Kintanar <bertrand@imakintanar.com>
 * @license    BSD License (3-clause)
 * @copyright  (c) 2017-2018, UPC Engineering, Inc.
 *
 * @link       https://gitlab.com/bkintanar/upcpi
 */

namespace Tests\Browser;

use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class LoginTest extends DuskTestCase
{
    /** @test */
    public function it_shows_email_not_registered_if_email_password_incorrect()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/login');
            $browser->type('email', 'test@test.com');
            $browser->type('password', 'password');
            $browser->press('Login');
            $browser->assertPathIs('/login');
            $browser->assertSee('The email "test@test.com" is not registered on this domain. Please login with another email.');
        });
    }

    /** @test */
    public function it_shows_email_not_registered_if_password_incorrect()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/login');
            $browser->type('email', 'bertrand@imakintanar.com');
            $browser->type('password', 'testtest');
            $browser->press('Login');
            $browser->assertPathIs('/login');
            $browser->assertSee('The email "bertrand@imakintanar.com" is not registered on this domain. Please login with another email.');
        });
    }

    /** @test */
    public function it_shows_member_list_if_login_is_successful()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/login');
            $browser->type('email', 'bertrand@imakintanar.com');
            $browser->type('password', 'retardko');
            $browser->press('Login');
            $browser->assertPathIs('/members');
        });
    }
}
