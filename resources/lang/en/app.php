<?php

/*
 * This file is part of the UPCPI Software package.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the 3-clause BSD License.
 *
 * This source file is subject to the 3-clause BSD License that is
 * bundled with this package in the LICENSE file.
 *
 * @version    alpha
 *
 * @author     Bertrand Kintanar <bertrand@imakintanar.com>
 * @license    BSD License (3-clause)
 * @copyright  (c) 2017-2018, UPC Engineering, Inc.
 *
 * @link       https://gitlab.com/bkintanar/upcpi
 */

return [
    'bro' => 'Bro.',
    'sis' => 'Sis.',
    'ptr' => 'Ptr.',
    'rev' => 'Rev.',

    'u_name'            => 'Name',
    'u_department'      => 'Department',
    'u_email'           => 'Email',
    'u_email_address'   => 'Email address',
    'u_mobile_phone'    => 'Mobile Phone',
    'u_date_of_birth'   => 'Date of Birth',
    'u_last_fellowship' => 'Last Fellowship',

    'u_edit' => 'Edit',
    'u_save' => 'Save',

    'u_members'     => 'Members',
    'u_my_info'     => 'My Info',
    'u_member_list' => 'Member List',

    'u_general'    => 'General',
    'u_membership' => 'Membership',

    'u_general_information' => 'General Information',
    'u_personal_details'    => 'Personal Details',
    'u_contact_details'     => 'Contact Details',
    'u_address_details'     => 'Address Details',
    'u_social_media'        => 'Social Media',

    'u_member_details'         => 'Member Details',
    'u_membership_information' => 'Membership Information',

    'u_first_name'       => 'First Name',
    'u_middle_name'      => 'Middle Name',
    'u_last_name'        => 'Last Name',
    'u_full_name'        => 'Full Name',
    'u_salutation'       => 'Salutation',
    'u_gender'           => 'Gender',
    'u_marital_status'   => 'Marital Status',
    'u_uuid'             => 'UUID',
    'u_status'           => 'Status',
    'u_nickname'         => 'Nickname',
    'u_home_phone'       => 'Home Phone',
    'u_personal_email'   => 'Personal Email',
    'u_address_1'        => 'Address 1',
    'u_address_2'        => 'Address 2',
    'u_city'             => 'City',
    'u_province'         => 'Province',
    'u_postcode'         => 'Postcode',
    'u_country'          => 'Country',
    'u_password'         => 'Password',
    'u_facebook'         => 'Facebook',
    'u_twitter'          => 'Twitter',
    'u_instagram'        => 'Instagram',
    'u_effective_date'   => 'Effective Date',
    'u_membership_title' => 'Membership Title',
    'u_reports_to'       => 'Reports To',
    'u_add_new_member'   => 'Add New Member',

    'u_male'   => 'Male',
    'u_female' => 'Female',

    'u_active'   => 'Active',
    'u_inactive' => 'Inactive',

    'u_reset_password'           => 'Reset password',
    'u_send_password_reset_link' => 'Send password reset link',

    'u_login'                   => 'Login',
    'u_or'                      => 'or',
    'u_scan_qr_code'            => 'Scan QR Code',
    'u_login_welcome'           => 'Welcome to UPC Portal',
    'u_forgot_password?'        => 'Forgot password?',
    'u_do_not_have_an_account?' => 'Do not have an account?',
    'u_create_an_account'       => 'Create an account',

    'u_spiritual_maturity'           => 'Spiritual Maturity',
    'u_water_baptism_date'           => 'Water Baptism Date',
    'u_holy_ghost_baptism_date'      => 'Holy Ghost Baptism Date',
    'u_last_holy_ghost_renewal_date' => 'Last Holy Ghost Renewal Date',

    'u_other_details'   => 'Other Details',
    'u_membership_date' => 'Membership Date',

    's_email_not_registered'      => 'The email ":email" is not registered on this domain. Please login with another email.',
    's_theres_no_update_for_type' => 'There\'s no update for :type,',
    's_add_new_update'            => 'Add new update',

    'l_membership_details' => 'membership details',
];
