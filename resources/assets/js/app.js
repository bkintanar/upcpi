/*
 *
 *   INSPINIA - Responsive Admin Theme
 *   version 2.7
 *
 */

$(document).ready(function () {


    // Add body-small class if window less than 768px
    if ($(this).width() < 769) {
        $('body').addClass('body-small')
    } else {
        $('body').removeClass('body-small')
    }

    // MetsiMenu
    $('#side-menu').metisMenu();

    // Collapse ibox function
    $('.collapse-link').on('click', function () {
        var ibox = $(this).closest('div.ibox');
        var button = $(this).find('i');
        var content = ibox.children('.ibox-content');
        content.slideToggle(200);
        button.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
        ibox.toggleClass('').toggleClass('border-bottom');
        setTimeout(function () {
            ibox.resize();
            ibox.find('[id^=map-]').resize();
        }, 50);
    });

    // Close ibox function
    $('.close-link').on('click', function () {
        var content = $(this).closest('div.ibox');
        content.remove();
    });

    // Fullscreen ibox function
    $('.fullscreen-link').on('click', function () {
        var ibox = $(this).closest('div.ibox');
        var button = $(this).find('i');
        $('body').toggleClass('fullscreen-ibox-mode');
        button.toggleClass('fa-expand').toggleClass('fa-compress');
        ibox.toggleClass('fullscreen');
        setTimeout(function () {
            $(window).trigger('resize');
        }, 100);
    });

    // Minimalize menu
    $('.navbar-minimalize').on('click', function () {
        $("body").toggleClass("mini-navbar");
        SmoothlyMenu();

    });

    // Full height of sidebar
    function fix_height() {
        var heightWithoutNavbar = $("body > #wrapper").height() - 61;
        $(".sidebard-panel").css("min-height", heightWithoutNavbar + "px");

        var navbarHeight = $('nav.navbar-default').height();
        var wrapperHeigh = $('#page-wrapper').height();

        if (navbarHeight > wrapperHeigh) {
            $('#page-wrapper').css("min-height", navbarHeight + "px");
        }

        if (navbarHeight < wrapperHeigh) {
            $('#page-wrapper').css("min-height", $(window).height() + "px");
        }

        if ($('body').hasClass('fixed-nav')) {
            if (navbarHeight > wrapperHeigh) {
                $('#page-wrapper').css("min-height", navbarHeight + "px");
            } else {
                $('#page-wrapper').css("min-height", $(window).height() - 60 + "px");
            }
        }

    }

    fix_height();

    // Fixed Sidebar
    $(window).bind("load", function () {
        if ($("body").hasClass('fixed-sidebar')) {
            $('.sidebar-collapse').slimScroll({
                height: '100%',
                railOpacity: 0.9
            });
        }
    });

    $(window).bind("load resize scroll", function () {
        if (!$("body").hasClass('body-small')) {
            fix_height();
        }
    });

    // Add slimscroll to element
    $('.full-height-scroll').slimscroll({
        height: '100%'
    })
});


// Minimalize menu when screen is less than 768px
$(window).bind("resize", function () {
    if ($(this).width() < 769) {
        $('body').addClass('body-small')
    } else {
        $('body').removeClass('body-small')
    }
});

function SmoothlyMenu() {
    if (!$('body').hasClass('mini-navbar') || $('body').hasClass('body-small')) {
        // Hide menu in order to smoothly turn on when maximize menu
        $('#side-menu').hide();
        // For smoothly turn on menu
        setTimeout(
            function () {
                $('#side-menu').fadeIn(400);
            }, 200);
    } else if ($('body').hasClass('fixed-sidebar')) {
        $('#side-menu').hide();
        setTimeout(
            function () {
                $('#side-menu').fadeIn(400);
            }, 100);
    } else {
        // Remove all inline style from jquery fadeIn function to reset menu state
        $('#side-menu').removeAttr('style');
    }
}

var forms = (function () {
    var body = $('body');
    var dropdown_form_wrapper = $('.dropdown-form-wrapper');
    var btns = dropdown_form_wrapper.find('.dropdown-btn');
    var forms = dropdown_form_wrapper.find('.dropdown-form');

    var toggleForm = function (e) {
        e.stopPropagation();
        var btn = $(this);
        var active = $('.dropdown-form.active');
        var form = btn.next();

        if (form.hasClass('active')) {
            btn.removeClass('active');
            form.removeClass('active');
            return
        }

        active.removeClass('active');
        btns.removeClass('active');
        form.addClass('active');
        btn.addClass('active')
    };

    var hideForm = function () {
        btns.removeClass('active')
        forms.removeClass('active');
    };

    var prevent = function (e) {
        e.stopPropagation();
    };

    body.click(hideForm);
    btns.click(toggleForm);
    forms.click(prevent);
})();

var validate = (function () {
    var forms = $('.validate');

    var validate = function (e) {
        var form = $(this);
        var data = form.serialize();
        var method = form.attr('method');
        var url = form.attr('action');

        if (!form.hasClass('complete')) {
            e.preventDefault();
        } else {
            return true;
        }

        $.ajax({
            type: method,
            data: data,
            url: url,
            success: function(data) {
                form.addClass('complete').submit();
            },
            error: function(data) {
                var response = data.responseJSON;
                var errors = response.errors;
                clearMessages(form)

                $.each(errors, function(key, error) {
                    var target = form.find(`[name=${key}]`)
                    target.after(`<p class="text-danger error-message">${error}</p>`)
                });
            }
        });
    };

    var clearMessages = function (form) {
        form.find('.error-message').remove();
    };

    forms.submit(validate);
})();
