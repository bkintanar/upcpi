<!-- Navigation -->
<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="clear">
                            <span class="block m-t-xs">
                                <strong class="font-bold">{{ Auth::user()->display_name }}</strong>
                            </span> <span class="text-muted text-xs block">Example menu <b class="caret"></b></span>
                        </span>
                    </a>
                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        <li>
                        	<a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-side').submit();">Logout</a>
			                <form id="logout-side" action="{{ route('logout') }}" method="POST" style="display: none;">
			                    {{ csrf_field() }}
			                </form>
                        </li>
                    </ul>
                </div>
                <div class="logo-element">
                    UPC
                </div>
            </li>
            <li class="{{ is_route_active('members.*') }}">
                <a href="#"><i class="fa fa-users"></i> <span class="nav-label">{{ __('app.u_members') }}</span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    @if(logged_member())
                    <li class="{{ is_route_active(['members.general.show', 'members.membership.show'], (isset($member) && $member->id == logged_member()->id)) }}">
                        <a href="{{ route('members.show', logged_member()) }}"><i class="fa fa-user-circle-o"></i> <span class="nav-label">{{ __('app.u_my_info') }}</span></a>
                    </li>
                    @endif
                    <li class="{{ is_route_active(['members.*'], (!isset($member) || $member->id != optional(logged_member())->id)) }}">
                        <a href="{{ route('members.index') }}"><i class="fa fa-list-ul"></i> <span class="nav-label">{{ __('app.u_member_list') }}</span></a>
                    </li>
                </ul>
            </li>
            <li class="{{ is_route_active('minor') }}">
                <a href="{{ url('/minor') }}"><i class="fa fa-th-large"></i> <span class="nav-label">Minor view</span></a>
            </li>
        </ul>
    </div>
</nav>
<!-- End navigation -->
