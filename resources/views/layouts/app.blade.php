<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title') - UPC Portal</title>

    <link rel="stylesheet" href="{!! mix('css/vendor.css') !!}"/>
    <link rel="stylesheet" href="{!! mix('css/app.css') !!}"/>
    <link rel="stylesheet" href="https://cdn.datatables.net/select/1.2.5/css/select.dataTables.min.css"/>
</head>
<body class="md-skin fixed-nav fixed-nav-basic fixed-sidebar">

<!-- Wrapper-->
<div id="wrapper">

    @include('layouts.navbar.side')

    <!-- Page wraper -->
    <div id="page-wrapper" class="gray-bg">

        @include('layouts.navbar.top')

        <!-- Page header -->
        @yield('page-header')

        <!-- Main view  -->
        @yield('content')

        @include('layouts.footer')

    </div>
    <!-- End page wrapper-->

</div>
<!-- End wrapper-->

<script src="{!! mix('js/app.js') !!}" type="text/javascript"></script>
@stack('scripts')

</body>
</html>
