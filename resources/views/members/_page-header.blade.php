<!-- members._page-header : start -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>{{ __('app.u_members') }}</h2>
        <ol class="breadcrumb">
            <li>
                <a href="index.html">Home</a>
            </li>
            <li>
                <a href="{{ route('members.index') }}" title="{{ __('app.u_members') }}">{{ __('app.u_members') }}</a>
            </li>
            <li class="active">
                <strong>
                    <a title="{{ $member->display_name }}"
                       href="{{ route('members.show', $member) }}">{{ $member->display_name }}</a>
                </strong>
            </li>
        </ol>
    </div>
</div>
<!-- members._page-header : end -->
