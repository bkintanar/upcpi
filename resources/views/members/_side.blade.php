<!-- members side : start -->
<div class="col-lg-2">
    <div class="ibox-content p-lg text-center">
        <div class="m-b-md">
            <h2 class="font-bold no-margins">
                {{ $member->display_name }}
            </h2>
            <small>{{ $member->department }} {{ __('app.u_department') }}</small>
        </div>
        {!! Avatar::create($member->display_name)->setDimension(140)->toSvg() !!}
{{--                    <img src="{{ Avatar::create($member->display_name)->setDimension(150)->toBase64() }}" width="140px" height="140px" class="img-circle circle-border m-b-md" />--}}
        {{--<img src="/images/avatars/{{ $image }}" width="140px" height="140px" class="img-circle circle-border m-b-md"--}}
             {{--alt="profile">--}}
        {{--{!! display_uuid($member->uuid) !!}--}}
        <div class="m-t-md">
            <span><i class="fa fa-envelope-o"></i></span> |
            <span><i class="fa fa-mobile-phone"></i></span> |
            <span><i class="fa fa-phone"></i></span> |
            <span><i class="fa fa-qrcode" data-toggle="modal" data-target="#myModal"></i></span>
        </div>
    </div>
	<div class="text-box no-padding">
	    <div class="p-m">
            <table class="table small m-b-xs">
                <tbody>
                <tr>
                    <td>
                        <strong>
                            <a href="{{ route('members.general.show', $member) }}">{{ strtoupper(__('app.u_general')) }}</a>
                        </strong>
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>
                            <a href="{{ route('members.membership.show', $member) }}">{{ strtoupper(__('app.u_membership')) }}</a>
                        </strong>
                    </td>
                </tr>
                </tbody>
            </table>
	    </div>
	</div>
</div>
<!-- members side : end -->
