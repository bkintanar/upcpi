<h3>{{ __('app.u_social_media') }}</h3>
<hr class="hr-line-solid">
<div class="form-horizontal">
    <div class="form-group">
        <label class="col-md-2 control-label">{{ __('app.u_facebook') }}</label>
        <div class="col-md-4">
            <p class="form-control-static">{{ $member->facebook }}</p>
        </div>
        <label class="col-md-2 control-label">{{ __('app.u_twitter') }}</label>
        <div class="col-md-4">
            <p class="form-control-static">{{ $member->twitter }}</p>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-2 control-label">{{ __('app.u_instagram') }}</label>
        <div class="col-md-4">
            <p class="form-control-static">{{ $member->instagram  }}</p>
        </div>
    </div>
</div>

@if(!$parent_loop->last)
    <hr class="hr-line-dashed">
@endif
