<div class="dropdown-form-wrapper">
    <button class="dropdown-btn btn btn-white">
        <i class="fa fa-pencil"></i>
        Edit
    </button>
    <form action="{{ route('members.general.update', compact('member')) }}" method="POST" class="dropdown-form validate">
        @csrf
        @method('patch')
        <div class="row">
            <div class="col-md-6">
                <label>{{ __('app.u_first_name') }}</label>
                <input type="text" class="form-control" name="first_name" value="{{ $member->first_name }}" />
            </div>
            <div class="col-md-6">
                <label>{{ __('app.u_middle_name') }}</label>
                <input type="text" class="form-control" name="middle_name" value="{{ $member->middle_name }}" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <label>{{ __('app.u_last_name') }}</label>
                <input type="text" class="form-control" name="last_name" value="{{ $member->last_name }}" />
            </div>
            <div class="col-md-6">
                <label>{{ __('app.u_salutation') }}</label>
                <input type="text" class="form-control" name="salutation" value="{{ $member->salutation }}" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <label>{{ __('app.u_gender') }}</label>
                @include('partials._input-select', ['name' => 'gender', 'model' => $member->gender, 'data' => ['M' => __('app.u_male'), 'F' => __('app.u_female')]])
            </div>
            <div class="col-md-6">
                <label>{{ __('app.u_marital_status') }}</label>
                @include('partials._input-select', ['name' => 'marital_status_id', 'model' => $member->marital_status_id, 'data' => [1 => __('app.u_single'), 2 => __('app.u_married')]])
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <label>{{ __('app.u_status') }}</label>
                @include('partials._input-select', ['name' => 'status', 'model' => $member->status, 'data' => ['active' => __('app.u_active'), 'inactive' => __('app.u_inactive')], 'disabled' => $member->user_id == auth()->user()->id])
            </div>
            <div class="col-md-6">
                <label>{{ __('app.u_nickname') }}</label>
                <input type="text" class="form-control" name="nickname" value="{{ $member->nickname }}" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 form-group" id="data_1">
                <label>{{ __('app.u_date_of_birth') }}</label>
                <div class="input-group date">
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control" name="date_of_birth" value="{{ optional($member->date_of_birth)->format('m/d/Y') }}">
                </div>
            </div>
        </div>
        <button type="submit" class="btn btn-success">{{ __('app.u_save') }}</button>
        <button type="" class="btn">{{ __('app.u_cancel') }}</button>
    </form>
    <h3>{{ __('app.u_personal_details') }}</h3>
    <hr class="hr-line-solid">
    <div class="form-horizontal">
        <div class="form-group">
            <label class="col-md-2 control-label">{{ __('app.u_full_name') }}</label>
            <div class="col-md-4">
                <p class="form-control-static">{{ $member->display_name }}</p>
            </div>
            <label class="col-md-2 control-label">{{ __('app.u_salutation') }}</label>
            <div class="col-md-4">
                <p class="form-control-static">{{ $member->salutation }}</p>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label">{{ __('app.u_gender') }}</label>
            <div class="col-md-4">
                <p class="form-control-static">{{ $member->gender == 'M' ? __('app.u_male') : __('app.u_female')  }}</p>
            </div>
            <label class="col-md-2 control-label">{{ __('app.u_marital_status') }}</label>
            <div class="col-md-4">
                <p class="form-control-static">{{ $member->marital_status }}</p>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label">{{ __('app.u_uuid') }}</label>
            <div class="col-md-4">
                <p class="form-control-static">{{ $member->uuid  }}</p>
            </div>
            <label class="col-md-2 control-label">{{ __('app.u_status') }}</label>
            <div class="col-md-4">
                <p class="form-control-static"><span class="badge badge-info">{{ __('app.u_'.$member->status) }}</span></p>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label">{{ __('app.u_nickname') }}</label>
            <div class="col-md-4">
                <p class="form-control-static">{{ $member->nickname  }}</p>
            </div>
            <label class="col-md-2 control-label">{{ __('app.u_date_of_birth') }}</label>
            <div class="col-md-4">
                <p class="form-control-static">{{ optional($member->date_of_birth)->toDateString() }}</p>
            </div>
        </div>
    </div>

    @if(!$parent_loop->last)
        <hr class="hr-line-dashed">
    @endif

</div>
