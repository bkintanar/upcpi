@foreach($member->addresses as $address)
<h3>{{ __('app.u_address_details') }} {{ $member->addresses->count() > 1 ? $loop->index+1 : '' }}</h3>
<hr class="hr-line-solid">
<div class="form-horizontal" id="address_details_form_{{$address->id}}">
    <div class="form-group">
        <label class="col-md-2 control-label">{{ __('app.u_address_1') }}</label>
        <div class="col-md-4">
            <p class="form-control-static">{{ $address->address_1 }}</p>
        </div>
        <label class="col-md-2 control-label">{{ __('app.u_address_2') }}</label>
        <div class="col-md-4">
            <p class="form-control-static">{{ $address->address_2 }}</p>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-2 control-label">{{ __('app.u_city') }}</label>
        <div class="col-md-4">
            <p class="form-control-static">{{ $address->city  }}</p>
        </div>
        <label class="col-md-2 control-label">{{ __('app.u_province') }}</label>
        <div class="col-md-4">
            <p class="form-control-static">{{ $address->province }}</p>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-2 control-label">{{ __('app.u_postcode') }}</label>
        <div class="col-md-4">
            <p class="form-control-static">{{ $address->postcode }}</p>
        </div>
        <label class="col-md-2 control-label">{{ __('app.u_country') }}</label>
        <div class="col-md-4">
            <p class="form-control-static">{{ $address->country  }}</p>
        </div>
    </div>
</div>
@if(!$loop->last)
    <hr class="hr-line-dashed">
@endif
@endforeach

@if($member->addresses->count() && !$parent_loop->last)
    <hr class="hr-line-dashed">
@endif
