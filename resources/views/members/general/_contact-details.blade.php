<div class="dropdown-form-wrapper">
    <button class="dropdown-btn btn btn-white">
        <i class="fa fa-pencil"></i>
        Edit
    </button>
    <form action="{{ route('members.general.update', compact('member')) }}" method="POST" class="dropdown-form">
        @csrf
        @method('patch')
        <div class="row">
            <div class="col-md-6">
                <label>{{ __('app.u_personal_email') }}</label>
                <input type="email" class="form-control" name="personal_email" value="{{ $member->personal_email }}" />
            </div>
            <div class="col-md-6">
                <label>{{ __('app.u_home_phone') }}</label>
                <input type="text" class="form-control" name="home_phone" value="{{ $member->home_phone }}" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <label>{{ __('app.u_mobile_phone') }}</label>
                <input type="text" class="form-control" name="mobile_phone" value="{{ $member->mobile_phone }}" />
            </div>
        </div>
        <button type="submit" class="btn btn-success">{{ __('app.u_save') }}</button>
    </form>
    <h3>{{ __('app.u_contact_details') }}</h3>
    <hr class="hr-line-solid">
    <div class="form-horizontal">
        <div class="form-group">
            <label class="col-md-2 control-label">{{ __('app.u_personal_email') }}</label>
            <div class="col-md-4">
                <p class="form-control-static">{{ $member->personal_email }}</p>
            </div>
            <label class="col-md-2 control-label">{{ __('app.u_home_phone') }}</label>
            <div class="col-md-4">
                <p class="form-control-static">{{ $member->home_phone }}</p>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label">{{ __('app.u_mobile_phone') }}</label>
            <div class="col-md-4">
                <p class="form-control-static">{{ $member->mobile_phone  }}</p>
            </div>
        </div>
    </div>

    @if(!$parent_loop->last)
        <hr class="hr-line-dashed">
    @endif
</div>
