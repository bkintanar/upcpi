<h3>{{ __('app.u_other_details') }}</h3>
<hr class="hr-line-solid">
<form accept-charset="UTF-8" class="form-horizontal" id="personal_details_form">
    <div class="form-group">
        <label class="col-md-2 control-label">{{ __('app.u_membership_date') }}</label>
        <div class="col-md-4">
            <p class="form-control-static">{{ optional(optional($member->spiritualMaturity)->membership_at)->toDateString() }}</p>
        </div>
    </div>
</form>
