<h3>{{ __('app.u_member_details') }}</h3>
<hr class="hr-line-solid">
@if($member->membershipDetail->count())
<div accept-charset="UTF-8" class="form-horizontal" id="address_details_form">
    <table class="table table-hover no-margins">
        <thead>
        <tr>
            <th>{{ __('app.u_effective_date') }}</th>
            <th>{{ __('app.u_department') }}</th>
            <th>{{ __('app.u_membership_title') }}</th>
            <th>{{ __('app.u_reports_to') }}</th>
        </tr>
        </thead>
        <tbody>
            @foreach($member->membershipDetail as $membershipDetail)
            <tr class="dropdown-form-wrapper">
                <td><small>{{ $membershipDetail->effective_date }}</small></td>
                <td>{{ optional($membershipDetail->department)->name }}</td>
                <td>{{ optional($membershipDetail->title)->name }}</td>
                <td></td>
                <td>
                    <button class="dropdown-btn btn btn-white">
                        <i class="fa fa-pencil"></i>
                        Edit
                    </button>
                    <form action="" class="dropdown-form">
                        <div class="row">
                            <div class="col-md-6">
                                <label>First Name</label>
                                <input type="text" class="form-control" />
                            </div>
                            <div class="col-md-6">
                                <label>Last Name</label>
                                <input type="text" class="form-control" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label>Email</label>
                                <input type="text" class="form-control" />
                            </div>
                            <div class="col-md-6">
                                <label>Address</label>
                                <input type="text" class="form-control" />
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </td>
            </tr>
            @endforeach
            <tr class="dropdown-form-wrapper">
                <td></td>
                <td></td>
                <td></td>
                <td>
                    <button class="dropdown-btn btn btn-white">
                        <i class="fa fa-pencil"></i>
                        Edit
                    </button>
                    <form action="" class="dropdown-form">
                        <div class="row">
                            <div class="col-md-6">
                                <label>First Name</label>
                                <input type="text" class="form-control" />
                            </div>
                            <div class="col-md-6">
                                <label>Last Name</label>
                                <input type="text" class="form-control" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label>Email</label>
                                <input type="text" class="form-control" />
                            </div>
                            <div class="col-md-6">
                                <label>Address</label>
                                <input type="text" class="form-control" />
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </td>
            </tr>
        </tbody>
    </table>
</div>
@else
<div class="sixteen wide column empty ">
    <p class="ui text medium text-center empty mt-10 mb-20">
        {{ __('app.s_theres_no_update_for_type', ['type' => __('app.l_membership_details')]) }}
        <b class="dropdown-form-button sub" data-target="#membershipDetail">{{ __('app.s_add_new_update') }}</b>
    </p>
    {{-- <hr class="primary two mb-30"> --}}
</div>
@endif
