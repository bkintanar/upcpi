<h3>{{ __('app.u_spiritual_maturity') }}</h3>
<hr class="hr-line-solid">
<form accept-charset="UTF-8" class="form-horizontal" id="contact_details_form">
    <div class="form-group">
        <label class="col-md-2 control-label">{{ __('app.u_water_baptism_date') }}</label>
        <div class="col-md-4">
            <p class="form-control-static">{{ optional(optional($member->spiritualMaturity)->water_baptism_at)->toDateString() }}</p>
        </div>
        <label class="col-md-2 control-label">{{ __('app.u_holy_ghost_baptism_date') }}</label>
        <div class="col-md-4">
            <p class="form-control-static">{{ optional(optional($member->spiritualMaturity)->holy_ghost_baptism_at)->toDateString() }}</p>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-2 control-label">{{ __('app.u_last_holy_ghost_renewal_date') }}</label>
        <div class="col-md-4">
            <p class="form-control-static">{{ optional(optional($member->spiritualMaturity)->last_holy_ghost_renewal_at)->toDateString()  }}</p>
        </div>
    </div>
</form>
