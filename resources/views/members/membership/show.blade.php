@extends('layouts.app')

@section('title', __('app.u_membership_information'))

@section('page-header')
    @include('members._page-header')
@endsection

@section('content')
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="modal inmodal" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content animated fadeIn">
                        <div class="modal-body">
                            <p class="text-center"> {!!  display_uuid($member->uuid) !!}</p>
                        </div>
                    </div>
                </div>
            </div>

        	@include('members._side')

            <div class="col-lg-10">
                    <div class="ibox float-e-margins">
                        <div class="ibox-content">
                            <h2>{{ __('app.u_membership_information') }}</h2>
                            <hr class="hr-line-dashed">

                            @foreach($partials as $partial)
                                @include($partial)
                                @if(!$loop->last)
                                    <hr class="hr-line-dashed">
                                @endif
                            @endforeach
                        </div>
                    </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
@endpush
