<tr>
    <td class="aligncenter">
        <a href="{{ $url }}">
            {{ $slot }}
        </a>
    </td>
</tr>
