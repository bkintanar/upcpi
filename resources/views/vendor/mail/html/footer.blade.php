<div class="footer">
    <table width="100%">
        <tr>
            <td class="aligncenter content-block">
                {{ Illuminate\Mail\Markdown::parse($slot) }}
            </td>
        </tr>
    </table>
</div>
