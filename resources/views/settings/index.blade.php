@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="col">
      <div class="row">
        <form action="{{ route('settings.update') }}" method="post">
          @csrf
          @method('PATCH')
          <div class="form-group">
            <label for="display_name">Display Name</label>
            <select id="display_name" class="form-control" name="display_name">
              @foreach ($options as $key => $value)
                <option value="{{ $key + 1 }}" {{ $displayName == $key + 1 ? 'selected' : null }}>{{ $value }}</option>
              @endforeach
            </select>
          </div>
          <div class="form-group">
            <button type="submit" class="btn btn-primary">Update</button>
          </div>
        </form>
      </div>
    </div>
  </div>
@endsection
