<select class="form-control" name="{{ $name }}" {{ (isset($disabled) && $disabled) ? 'disabled' : '' }}>
    <option value='' @if($model == null) selected @endif>-- Select One --</option>
    @foreach($data as $key => $value)
    <option value='{{ $key }}' @if($model == $key) selected @endif>{{ $value }}</option>
    @endforeach
</select>
