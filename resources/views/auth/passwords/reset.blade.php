<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>{{ __('app.u_reset_password') }} - UPC Portal</title>

    <link rel="stylesheet" href="{!! mix('css/vendor.css') !!}"/>
    <link rel="stylesheet" href="{!! mix('css/app.css') !!}"/>

</head>

<body class="md-skin">

<div class="passwordBox animated fadeInDown">
    <div class="row">

        <div class="col-md-12">
            <div class="ibox-content">

                <h2 class="font-bold">{{ __('app.u_reset_password') }}</h2>

                <div class="row">

                    <div class="col-lg-12">
                        <form class="m-t" role="form" method="POST" action="{{ route('password.request') }}">
                            @csrf

                            <input type="hidden" name="token" value="{{ $token }}">

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' has-error' : '' }}" name="email" value="{{ old('email') }}" placeholder="{{ __('app.u_email_address') }}" required>
                                @if($errors->has('email'))
                                    <span class="help-block text-danger">{{ __('passwords.user') }}</span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' has-error' : '' }}" name="password" placeholder="Password" required>
                                @if($errors->has('password'))
                                    <span class="help-block text-danger">{{ $errors->first('password') }}</span>
                                @endif
                            </div>

                            <div class="form-group">
                                <input id="password_confirmation" type="password" class="form-control" name="password_confirmation" placeholder="Confirm Password" required>
                            </div>

                            <button type="submit" class="btn btn-primary block full-width m-b"><i class="fa fa-refresh"></i> {{ strtoupper(__('app.u_reset_password')) }}</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr/>
    <div class="row">
        <div class="col-md-6">
            Copyright UPC Engineering, Inc.
        </div>
        <div class="col-md-6 text-right">
            <small>© 2017-2018</small>
        </div>
    </div>
</div>

</body>

</html>
