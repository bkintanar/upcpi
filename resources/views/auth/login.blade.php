<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>{{ __('app.u_login') }} - UPC Portal</title>

    <link rel="stylesheet" href="{!! mix('css/vendor.css') !!}"/>
    <link rel="stylesheet" href="{!! mix('css/app.css') !!}"/>

</head>

<body class="md-skin">
    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
            <div>

                <br />
                <br />
                {{-- {!! media('login-logo.png', '170px') !!} --}}
                <br />
                <br />
            </div>
            <h3>{{ __('app.u_login_welcome') }}</h3>
            {{ config('app.url') }}
            <p>
                <!--Continually expanded and constantly improved Inspinia Admin Them (IN+)-->
            </p>
            <form class="m-t" role="form" method="POST" action="{{ route('login') }}">
                @csrf
                @if($errors->has('email'))
                    <span class="help-block has-error text-danger">{{ __('app.s_email_not_registered', ['email' => old('email')]) }}</span>
                @endif
                <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                    <input type="email" autofocus class="form-control" placeholder="{{ __('app.u_email') }}" name="email" id="email" value="{{old('email')}}" required="">
                </div>
                <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                    <input type="password" class="form-control" placeholder="{{ __('app.u_password') }}" name="password" id="password" value="{{ old('password') }}" required="">
                </div>
                <button type="submit" class="btn btn-primary block full-width m-b"><i class="fa fa-sign-in"></i> {{ __('app.u_login') }}</button>

                <p>{{ __('app.u_or') }}</p>

                <button type="button" class="btn btn-info block full-width m-b" id="qrCodeButton" data-toggle="modal" data-target="#instascanModal"><i class="fa fa-qrcode"></i> {{ __('app.u_scan_qr_code') }}</button>

                <a href="{{ route('password.request') }}">
                    <small>{{ __('app.u_forgot_password?') }}</small>
                </a>
                <p class="text-muted text-center">
                    <small>{{ __('app.u_do_not_have_an_account?') }}</small>
                </p>
                <a class="btn btn-sm btn-white btn-block" href="register.html">{{ __('app.u_create_an_account') }}</a>
            </form>

            <p class="m-t">
                <small>UPC Engineering, Inc. &copy; 2017-2018</small>
            </p>
        </div>
    </div>

    <div class="modal inmodal" id="instascanModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content animated fadeIn">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title"><i class="fa fa-qrcode"></i> Scan QR Code</h4>
                    <small class="font-bold">You may login using your personal QR Code that was provided to you by the system.</small>
                </div>
                <div class="modal-body">
                    <div class="qrcode-wrapper">
                        <div class="focus">
                            <div class="top-left"></div>
                            <div class="top-right"></div>
                            <div class="bot-left"></div>
                            <div class="bot-right"></div>
                        </div>
                        <div class="video-wrapper">
                            <video id="qrCodeScanner" height="500px"></video>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Mainly scripts -->
    <script src="{{ mix('js/app.js') }}"></script>
    <script src="/js/instascan.min.js"></script>
    <script>
        const button = $('#qrCodeButton');
        const video = $('#qrCodeScanner');

        let scanner = new Instascan.Scanner({ video: video[0] });

        scanner.addListener('scan', function (uuid) {
            $.ajax({
                type: "POST",
                url: "/qrcode-login",
                data: {uuid: uuid, _token: "{{csrf_token()}}" },
                success: function(response) {
                    location.reload();
                }
            });
        });

        const showVideo = () => {
            button.hide();
            video.show();

            Instascan.Camera.getCameras().then(function (cameras) {
                if (cameras.length > 0) {
                    scanner.start(cameras[0]);
                } else {
                    console.error('No cameras found.');
                }
                }).catch(function (e) {
                console.error(e);
            });
        }

        button.click(showVideo);

        $('#instascanModal').on('hidden.bs.modal', function () {
            scanner.stop();
        });

    </script>
</body>

</html>
