<?php

/*
 * This file is part of the UPCPI Software package.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the 3-clause BSD License.
 *
 * This source file is subject to the 3-clause BSD License that is
 * bundled with this package in the LICENSE file.
 *
 * @version    alpha
 *
 * @author     Bertrand Kintanar <bertrand@imakintanar.com>
 * @license    BSD License (3-clause)
 * @copyright  (c) 2017-2018, UPC Engineering, Inc.
 *
 * @link       https://gitlab.com/bkintanar/upcpi
 */

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SpatiePermissionTablesSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->rolesRun();
        $this->permissionsRun();
        $this->modelHasRolesRun();
        $this->roleHasPermissionsRun();
    }

    public function modelHasRolesRun()
    {
        DB::table('model_has_roles')->delete();

        DB::table('model_has_roles')->insert([
            [
                'role_id'    => 1,
                'model_type' => 'UPCEngineering\Eloquent\User',
                'model_id'   => 1,
            ],
        ]);
    }

    public function permissionsRun()
    {
        $time = Carbon::now();

        DB::table('permissions')->delete();

        DB::table('permissions')->insert([
            [
                'id'         => 1,
                'name'       => 'members.first_name.show',
                'guard_name' => 'web',
                'created_at' => $time,
                'updated_at' => $time,
            ],
            [
                'id'         => 2,
                'name'       => 'members.middle_name.show',
                'guard_name' => 'web',
                'created_at' => $time,
                'updated_at' => $time,
            ],
            [
                'id'         => 3,
                'name'       => 'members.last_name.show',
                'guard_name' => 'web',
                'created_at' => $time,
                'updated_at' => $time,
            ],
            [
                'id'         => 4,
                'name'       => 'members.ess.index',
                'guard_name' => 'web',
                'created_at' => $time,
                'updated_at' => $time,
            ],
        ]);
    }

    public function roleHasPermissionsRun()
    {
        DB::table('role_has_permissions')->delete();

        DB::table('role_has_permissions')->insert([
            [
                'permission_id' => 1,
                'role_id'       => 1,
            ],
            [
                'permission_id' => 2,
                'role_id'       => 1,
            ],
            [
                'permission_id' => 3,
                'role_id'       => 1,
            ],
            [
                'permission_id' => 4,
                'role_id'       => 1,
            ],
            [
                'permission_id' => 1,
                'role_id'       => 2,
            ],
            [
                'permission_id' => 2,
                'role_id'       => 2,
            ],
            [
                'permission_id' => 3,
                'role_id'       => 2,
            ],
            [
                'permission_id' => 4,
                'role_id'       => 2,
            ],
            [
                'permission_id' => 4,
                'role_id'       => 3,
            ],
        ]);
    }

    public function rolesRun()
    {
        $time = Carbon::now();

        DB::table('roles')->delete();

        DB::table('roles')->insert([
            [
                'id'         => 1,
                'name'       => 'super-admin',
                'guard_name' => 'web',
                'created_at' => $time,
                'updated_at' => $time,
            ],
            [
                'id'         => 2,
                'name'       => 'host-pastor',
                'guard_name' => 'web',
                'created_at' => $time,
                'updated_at' => $time,
            ],
            [
                'id'         => 3,
                'name'       => 'member-self-service',
                'guard_name' => 'web',
                'created_at' => $time,
                'updated_at' => $time,
            ],
        ]);
    }
}
