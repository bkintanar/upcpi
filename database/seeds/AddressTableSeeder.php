<?php

/*
 * This file is part of the UPCPI Software package.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the 3-clause BSD License.
 *
 * This source file is subject to the 3-clause BSD License that is
 * bundled with this package in the LICENSE file.
 *
 * @version    alpha
 *
 * @author     Bertrand Kintanar <bertrand@imakintanar.com>
 * @license    BSD License (3-clause)
 * @copyright  (c) 2017-2018, UPC Engineering, Inc.
 *
 * @link       https://gitlab.com/bkintanar/upcpi
 */

use Carbon\Carbon;
use Faker\Generator as Faker;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\ConsoleOutput;

class AddressTableSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
        $output = new ConsoleOutput();
        $progress = new ProgressBar($output, 250);

        DB::table('addresses')->delete();

        DB::table('addresses')->insert([
            [
                'id'             => 1,
                'member_id'      => 1,
                'address_1'      => 'Judge Pedro Son Compound',
                'address_2'      => 'Minoza St, Talamban',
                'city'           => 'Cebu City',
                'province'       => 'Cebu',
                'postcode'       => 6000,
                'country'        => 'Philippines',
                'created_at'     => Carbon::now(),
                'updated_at'     => Carbon::now(),
            ],
        ]);

        $progress->advance();

        $progress->start();
        for ($i = 2; $i <= 250; $i++) {
            DB::table('addresses')->insert([
                [
                    'id'             => $i,
                    'member_id'      => $i,
                    'address_1'      => $faker->streetAddress,
                    'address_2'      => $faker->streetAddress,
                    'city'           => $faker->city,
                    'province'       => $faker->city,
                    'postcode'       => $faker->postcode,
                    'country'        => $faker->country,
                    'created_at'     => Carbon::now(),
                    'updated_at'     => Carbon::now(),
                ],
            ]);

            $progress->advance();
        }

        $progress->finish();
    }
}
