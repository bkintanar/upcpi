<?php

/*
 * This file is part of the UPCPI Software package.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the 3-clause BSD License.
 *
 * This source file is subject to the 3-clause BSD License that is
 * bundled with this package in the LICENSE file.
 *
 * @version    alpha
 *
 * @author     Bertrand Kintanar <bertrand@imakintanar.com>
 * @license    BSD License (3-clause)
 * @copyright  (c) 2017-2018, UPC Engineering, Inc.
 *
 * @link       https://gitlab.com/bkintanar/upcpi
 */

namespace UPCEngineering\Eloquent;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravolt\Avatar\Avatar;
use UPCEngineering\Eloquent\Concerns\HasNameAttribute;
use UPCEngineering\Traits\UsesSearchableTenantConnection;

class Member extends Authenticatable
{
    use HasNameAttribute, SoftDeletes, UsesSearchableTenantConnection;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'members';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'uuid',
        'user_id',
        'department_id',
        'first_name',
        'middle_name',
        'last_name',
        'gender',
        'marital_status_id',
        'salutation',
        'nickname',
        'date_of_birth',
        'personal_email',
        'home_phone',
        'mobile_phone',
    ];

    protected $appends = ['display_name', 'marital_status', 'department', 'path', 'image_path'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['date_of_birth', 'deleted_at'];

    /**
     * Get the route key for the model.
     *
     * @return string
     *
     * @author Bertrand Kintanar <bertrand@imakintanar.com>
     */
    public function getRouteKeyName()
    {
        return 'uuid';
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     *
     * @author Bertrand Kintanar <bertrand@imakintanar.com>
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     *
     * @author Bertrand Kintanar <bertrand@imakintanar.com>
     */
    public function getMaritalStatusAttribute()
    {
        $relation = $this->hasOne(MaritalStatus::class, 'id', 'marital_status_id')->first();

        return $this->attributes['marital_status'] = optional($relation)->name;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     *
     * @author Bertrand Kintanar <bertrand@imakintanar.com>
     */
    public function getDepartmentAttribute()
    {
        $relation = $this->hasOne(Department::class, 'id', 'department_id')->first();

        return $this->attributes['department'] = optional($relation)->name;
    }

    /**
     * @return string
     *
     * @author Bertrand Kintanar <bertrand@imakintanar.com>
     */
    public function getPathAttribute()
    {
        return route('members.show', $this->attributes['uuid']);
    }

    public function getImagePathAttribute()
    {
        $avatar = new Avatar(['fontSize' => 18, 'backgrounds' => [
            '#f44336',
            '#E91E63',
            '#9C27B0',
            '#673AB7',
            '#3F51B5',
            '#2196F3',
            '#03A9F4',
            '#00BCD4',
            '#009688',
            '#4CAF50',
            '#8BC34A',
            '#CDDC39',
            '#FFC107',
            '#FF9800',
            '#FF5722',
        ]]);

        return $avatar->create($this->display_name)->setDimension(40)->toBase64()->__toString();
    }

    public function toSearchableArray()
    {
        return $this->toArray() + ['path' => route('members.show', $this)];
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     *
     * @author Bertrand Kintanar <bertrand@imakintanar.com>
     */
    public function addresses()
    {
        return $this->hasMany(Address::class);
    }

    public function maritalStatus()
    {
        return $this->belongsTo(MaritalStatus::class);
    }

    public function department()
    {
        return $this->belongsTo(Department::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     *
     * @author Bertrand Kintanar <bertrand@imakintanar.com>
     */
    public function membershipDetail()
    {
        return $this->hasMany(MembershipDetail::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     *
     * @author Bertrand Kintanar <bertrand@imakintanar.com>
     */
    public function spiritualMaturity()
    {
        return $this->hasOne(SpiritualMaturity::class);
    }
}
