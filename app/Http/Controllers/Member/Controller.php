<?php

/*
 * This file is part of the UPCPI Software package.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the 3-clause BSD License.
 *
 * This source file is subject to the 3-clause BSD License that is
 * bundled with this package in the LICENSE file.
 *
 * @version    alpha
 *
 * @author     Bertrand Kintanar <bertrand@imakintanar.com>
 * @license    BSD License (3-clause)
 * @copyright  (c) 2017-2018, UPC Engineering, Inc.
 *
 * @link       https://gitlab.com/bkintanar/upcpi
 */

namespace UPCEngineering\Http\Controllers\Member;

use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use Ramsey\Uuid\Uuid;
use UPCEngineering\Eloquent\Member;
use UPCEngineering\Http\Controllers\Controller as BaseController;
use UPCEngineering\Http\Requests\MemberRequest;

class Controller extends BaseController
{
    /**
     * @param MemberRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|View
     *
     * @author Bertrand Kintanar <bertrand@imakintanar.com>
     */
    public function index(MemberRequest $request): View
    {
        return view('members.index');
    }

    /**
     * @param MemberRequest $request
     * @param Member        $member
     *
     * @return \Illuminate\Http\RedirectResponse
     *
     * @author Bertrand Kintanar <bertrand@imakintanar.com>
     */
    public function show(MemberRequest $request, Member $member): RedirectResponse
    {
        return redirect()->route('members.general.show', compact('member'));
    }

    /**
     * @param MemberRequest $request
     *
     * @return Member
     *
     * @author Bertrand Kintanar <bertrand@imakintanar.com>
     */
    public function store(MemberRequest $request): Member
    {
        $data = $request->all();

        $data['status'] = array_key_exists('status', $data) ? 'active' : 'inactive';
        $data['uuid'] = Uuid::uuid4()->toString();

        return Member::create($data);
    }

    public function create(MemberRequest $request)
    {
        return view('members.create');
    }
}
