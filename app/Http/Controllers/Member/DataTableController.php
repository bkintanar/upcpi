<?php

/*
 * This file is part of the UPCPI Software package.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the 3-clause BSD License.
 *
 * This source file is subject to the 3-clause BSD License that is
 * bundled with this package in the LICENSE file.
 *
 * @version    alpha
 *
 * @author     Bertrand Kintanar <bertrand@imakintanar.com>
 * @license    BSD License (3-clause)
 * @copyright  (c) 2017-2018, UPC Engineering, Inc.
 *
 * @link       https://gitlab.com/bkintanar/upcpi
 */

namespace UPCEngineering\Http\Controllers\Member;

use UPCEngineering\Eloquent\Member;
use UPCEngineering\Http\Controllers\Controller as BaseController;
use UPCEngineering\Http\Requests\MemberRequest;

class DataTableController extends BaseController
{
    /**
     * @param MemberRequest $request
     *
     * @throws \Exception
     *
     * @return mixed
     *
     * @author Bertrand Kintanar <bertrand@imakintanar.com>
     */
    public function index(MemberRequest $request)
    {
        $members = Member::with(['maritalStatus', 'department'])->get();

        return datatables()->of($members)->make(true);
    }
}
