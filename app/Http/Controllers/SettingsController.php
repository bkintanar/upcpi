<?php

/*
 * This file is part of the UPCPI Software package.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the 3-clause BSD License.
 *
 * This source file is subject to the 3-clause BSD License that is
 * bundled with this package in the LICENSE file.
 *
 * @version    alpha
 *
 * @author     Bertrand Kintanar <bertrand@imakintanar.com>
 * @license    BSD License (3-clause)
 * @copyright  (c) 2017-2018, UPC Engineering, Inc.
 *
 * @link       https://gitlab.com/bkintanar/upcpi
 */

namespace UPCEngineering\Http\Controllers;

use Illuminate\Http\Request;

class SettingsController extends Controller
{
    public function show(Request $request)
    {
        $user = $request->user();
        $displayName = user_setting('app.display-name');
        $showSalutation = user_setting('app.show-salutation');

        $args = [$user->toArray(), null, $showSalutation];

        $options = [];
        foreach ($user->variants as $key => $variant) {
            $args[1] = $key;
            $option = (new $variant(...$args))->handle();
            $options[] = $option;
        }

        return view('settings.index', compact(['displayName', 'user', 'options']));
    }

    public function update(Request $request)
    {
        user_setting('app.display-name', $request->display_name);

        return redirect()->back();
    }
}
