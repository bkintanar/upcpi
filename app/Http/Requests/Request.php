<?php

/*
 * This file is part of the UPCPI Software package.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the 3-clause BSD License.
 *
 * This source file is subject to the 3-clause BSD License that is
 * bundled with this package in the LICENSE file.
 *
 * @version    alpha
 *
 * @author     Bertrand Kintanar <bertrand@imakintanar.com>
 * @license    BSD License (3-clause)
 * @copyright  (c) 2017-2018, UPC Engineering, Inc.
 *
 * @link       https://gitlab.com/bkintanar/upcpi
 */

namespace UPCEngineering\Http\Requests;

class Request extends FormRequest
{
    /**
     * @var array
     */
    protected $methods = [
        'GET'    => ['index', 'create', 'show', 'edit'],
        'POST'   => ['store'],
        'PUT'    => ['update'],
        'PATCH'  => ['update'],
        'DELETE' => ['destroy'],
    ];

    /**
     * Authorize a given action for the current user.
     *
     * @return bool
     *
     * @author Bertrand Kintanar <bertrand@imakintanar.com>
     */
    public function authorize()
    {
        $permission = $this->route()->getName();

        return $this->can($permission);
    }

    /**
     * @param $permission
     *
     * @return bool
     *
     * @author Bertrand Kintanar <bertrand@imakintanar.com>
     */
    public function can($permission)
    {
        $action = substr($permission, strrpos($permission, '.') + 1);

        return in_array($action, $this->methods[$this->method()]) && auth()->user()->can($permission, $this->member ?? null);
    }
}
