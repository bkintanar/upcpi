<?php

/*
 * This file is part of the UPCPI Software package.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the 3-clause BSD License.
 *
 * This source file is subject to the 3-clause BSD License that is
 * bundled with this package in the LICENSE file.
 *
 * @version    alpha
 *
 * @author     Bertrand Kintanar <bertrand@imakintanar.com>
 * @license    BSD License (3-clause)
 * @copyright  (c) 2017-2018, UPC Engineering, Inc.
 *
 * @link       https://gitlab.com/bkintanar/upcpi
 */

namespace UPCEngineering\Http\Requests;

class AddressRequest extends Request
{
    /**
     * Rules for each request method.
     *
     * @var array
     */
    protected $rules = [
        'GET'  => [],
        'POST' => [
            'member_id'   => ['exists:tenant.members,id'],
            'address_1'   => ['required', 'max:255'],
            'city'        => ['required', 'max:255'],
            'country'     => ['required', 'code'],
        ],
    ];

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     *
     * @author Bertrand Kintanar <bertrand@imakintanar.com>
     */
    public function rules()
    {
        return $this->rules[$this->method()];
    }
}
