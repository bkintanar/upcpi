<?php

/*
 * This file is part of the UPCPI Software package.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the 3-clause BSD License.
 *
 * This source file is subject to the 3-clause BSD License that is
 * bundled with this package in the LICENSE file.
 *
 * @version    alpha
 *
 * @author     Bertrand Kintanar <bertrand@imakintanar.com>
 * @license    BSD License (3-clause)
 * @copyright  (c) 2017-2018, UPC Engineering, Inc.
 *
 * @link       https://gitlab.com/bkintanar/upcpi
 */

namespace UPCEngineering\Http\Requests;

class MemberRequest extends Request
{
    /**
     * Rules for each request method.
     *
     * @var array
     */
    protected $rules = [
        'GET'  => [],
        'POST' => [
            'email' => ['email'],
        ],
        'PATCH' => [
            'first_name'    => 'required',
            'last_name'     => 'required',
            'gender'        => 'required',
            'date_of_birth' => ['required', 'date_format:m/d/Y'],
        ],
    ];

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     *
     * @author <bertrand@imakintanar.com>
     */
    public function rules()
    {
        return $this->rules[$this->method()];
    }
}
