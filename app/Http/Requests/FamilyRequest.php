<?php

/*
 * This file is part of the UPCPI Software package.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the 3-clause BSD License.
 *
 * This source file is subject to the 3-clause BSD License that is
 * bundled with this package in the LICENSE file.
 *
 * @version    alpha
 *
 * @author     Bertrand Kintanar <bertrand@imakintanar.com>
 * @license    BSD License (3-clause)
 * @copyright  (c) 2017-2018, UPC Engineering, Inc.
 *
 * @link       https://gitlab.com/bkintanar/upcpi
 */

namespace UPCEngineering\Http\Requests;

use Illuminate\Validation\Rule;

class FamilyRequest extends Request
{
    /**
     * Rules for each request method.
     *
     * @var array
     */
    protected $rules = [
        'GET'  => [],
        'POST' => [
            'last_name' => ['required'],
        ],
    ];

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $last_name = $this->last_name;

        if ($this->method() == 'POST') {
            $this->rules['POST']['first_name'] = [
                'required',
                Rule::exists('tenant.members')->where(function ($query) use ($last_name) {
                    $query->where('last_name', $last_name);
                }),
            ];
        }

        return $this->rules[$this->method()];
    }
}
