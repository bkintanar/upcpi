<?php

/*
 * This file is part of the UPCPI Software package.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the 3-clause BSD License.
 *
 * This source file is subject to the 3-clause BSD License that is
 * bundled with this package in the LICENSE file.
 *
 * @version    alpha
 *
 * @author     Bertrand Kintanar <bertrand@imakintanar.com>
 * @license    BSD License (3-clause)
 * @copyright  (c) 2017-2018, UPC Engineering, Inc.
 *
 * @link       https://gitlab.com/bkintanar/upcpi
 */

namespace UPCEngineering\Http\Requests;

use Illuminate\Foundation\Http\FormRequest as Request;

abstract class FormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return array
     *
     * @author Bertrand Kintanar <bertrand@imakintanar.com>
     */
    public function rules()
    {
        return [];
    }

    /**
     * Validate the class instance.
     *
     * @return void
     */
    public function validate()
    {
        parent::validate();

        if ($this->getValidatorInstance()->passes() && $this->ajax() && !$this->wantsJson() && $this->request->get('validate')) {
            die;
        }
    }
}
