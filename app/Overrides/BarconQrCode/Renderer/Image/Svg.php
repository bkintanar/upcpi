<?php

/*
 * This file is part of the UPCPI Software package.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the 3-clause BSD License.
 *
 * This source file is subject to the 3-clause BSD License that is
 * bundled with this package in the LICENSE file.
 *
 * @version    alpha
 *
 * @author     Bertrand Kintanar <bertrand@imakintanar.com>
 * @license    BSD License (3-clause)
 * @copyright  (c) 2017-2018, UPC Engineering, Inc.
 *
 * @link       https://gitlab.com/bkintanar/upcpi
 */

namespace UPCEngineering\Overrides\BaconQrCode\Renderer\Image;

use BaconQrCode\Renderer\Image\Svg as BaconQrCodeSvg;

/**
 * SVG backend.
 */
class Svg extends BaconQrCodeSvg
{
    const UPPER_LEFT = 1;
    const LOWER_LEFT = 2;
    const UPPER_RIGHT = 3;
    const LOWER_RIGHT = 4;
    const IMAGE_CENTER = 5;

    /**
     * Starting point of the svg to create.
     *
     * @var int
     */
    protected $basePoint = 0;

    /**
     * drawBlock(): defined by RendererInterface.
     *
     * @see    ImageRendererInterface::drawBlock()
     *
     * @param int    $x
     * @param int    $y
     * @param string $colorId
     *
     * @return void
     */
    public function drawBlock($x, $y, $colorId)
    {
        $this->basePoint = $this->basePoint == 0 ? $x : $this->basePoint;

        if ($this->position(self::UPPER_LEFT, $x, $y) || $this->position(self::LOWER_LEFT, $x, $y)
            || $this->position(self::UPPER_RIGHT, $x, $y) || $this->position(self::LOWER_RIGHT, $x, $y)) {
            $adjustment = (float) ($this->blockSize / 2);

            parent::drawBlock($x - $adjustment, $y - $adjustment, $colorId);

            return;
        }

        if ($this->position(self::IMAGE_CENTER, $x, $y)) {

            // intentionally left blank.

            return;
        }

        $circle = $this->svg->addChild('circle');
        $circle->addAttribute('cx', $x);
        $circle->addAttribute('cy', $y);
        $circle->addAttribute('r', $this->blockSize / 2);
        $circle->addAttribute('fill', '#'.$this->colors[$colorId]);
    }

    /**
     * @param string $orientation
     * @param int    $x
     * @param int    $y
     *
     * @return bool
     */
    protected function position($orientation, $x, $y)
    {
        switch ($orientation) {
            case self::UPPER_LEFT:
                return $x >= $this->basePoint && $x <= ($this->basePoint + ($this->blockSize * 6)) && $y >= $this->basePoint && $y <= ($this->basePoint + ($this->blockSize * 6));
            case self::LOWER_LEFT:
                return $x >= $this->basePoint && $x <= ($this->basePoint + ($this->blockSize * 6)) && $y >= ($this->basePoint + ($this->blockSize * 30)) && $y <= ($this->basePoint + ($this->blockSize * 36));
            case self::UPPER_RIGHT:
                return $x >= ($this->basePoint + ($this->blockSize * 30)) && $x <= ($this->basePoint + ($this->blockSize * 36)) && $y >= $this->basePoint && $y <= ($this->basePoint + ($this->blockSize * 6));
            case self::LOWER_RIGHT:
                return $x >= ($this->basePoint + ($this->blockSize * 28)) && $x <= ($this->basePoint + ($this->blockSize * 32)) && $y >= ($this->basePoint + ($this->blockSize * 28)) && $y <= ($this->basePoint + ($this->blockSize * 32));
            case self::IMAGE_CENTER:
                return $x >= ($this->basePoint + ($this->blockSize * 11)) && $x <= ($this->basePoint + ($this->blockSize * 25)) && $y >= ($this->basePoint + ($this->blockSize * 11)) && $y <= ($this->basePoint + ($this->blockSize * 25));
            default:
                return false;
        }
    }
}
