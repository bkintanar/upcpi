<?php

/*
 * This file is part of the UPCPI Software package.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the 3-clause BSD License.
 *
 * This source file is subject to the 3-clause BSD License that is
 * bundled with this package in the LICENSE file.
 *
 * @version    alpha
 *
 * @author     Bertrand Kintanar <bertrand@imakintanar.com>
 * @license    BSD License (3-clause)
 * @copyright  (c) 2017-2018, UPC Engineering, Inc.
 *
 * @link       https://gitlab.com/bkintanar/upcpi
 */

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Route;
use UPCEngineering\Eloquent\UserSetting;

if (!function_exists('display_uuid')) {
    function display_uuid($uuid)
    {
        return QrCode::format('svg')->size(200)->errorCorrection('H')->margin('1')->encoding('UTF-8')->generate($uuid);
    }
}

if (!function_exists('display_uuid_png')) {
    function display_uuid_png($uuid)
    {
        $qrCode = QrCode::format('png')
            ->color(26, 179, 148)
            ->size(200)
            ->errorCorrection('H')
            ->margin('1')
            ->encoding('UTF-8')
            ->merge('/public/images/logo.png', .38)
            ->generate($uuid);

        $uuid_png = base64_encode($qrCode);

        return sprintf("<img src='data:image/png;base64,%s' />", $uuid_png);
    }
}

if (!function_exists('is_route_active')) {
    function is_route_active($routes, $condition = true)
    {
        $output = 'active';

        if (is_string($routes)) {
            $routes = [$routes];
        }

        foreach ($routes as $route) {
            if (Route::currentRouteNamed($route) && $condition) {
                return $output;
            }
        }
    }
}

if (!function_exists('logged_member')) {
    function logged_member()
    {
        return auth()->user()->member;
    }
}

if (!function_exists('media')) {
    function media($filename, $height)
    {
        $directory = app(\Hyn\Tenancy\Website\Directory::class);
        $tenancy = app(\Hyn\Tenancy\Environment::class);

        $uuid = $tenancy->website()->uuid;

        $directory->setWebsite($tenancy->website());

        if (Cache::tags([$uuid])->has('logo')) {
            $content = Cache::tags([$uuid])->get('logo');
        } else {
            if ($directory->exists("media/{$filename}")) {
                $content = $directory->get("media/{$filename}");

                Cache::tags([$uuid])->forever('logo', $content);
            } else {
                $imagePath = public_path('/images/default-upci-logo.png');
                $content = file_get_contents($imagePath);
            }
        }

        $image = base64_encode($content);
        // $mime = mime_content_type($content);

        return sprintf("<img src='data:%s;base64,%s' height='%s' />", 'png', $image, $height);
    }
}

if (!function_exists('user_setting')) {
    function user_setting($key, $value = null)
    {
        $user = auth()->user();

        if (!$user) {
            return;
        }

        $user_id = Auth::user()->id;

        if (is_null($value)) {
            $userSetting = UserSetting::where('user_id', $user_id)->where('key', $key)->first();

            if ($userSetting) {
                return $userSetting->value;
            }

            return;
        }

        $userSetting = UserSetting::firstOrNew([
            'user_id' => $user_id,
            'key'     => $key,
        ]);

        $userSetting->value = $value;
        $userSetting->save();

        return $value;
    }
}
