<?php

/*
 * This file is part of the UPCPI Software package.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the 3-clause BSD License.
 *
 * This source file is subject to the 3-clause BSD License that is
 * bundled with this package in the LICENSE file.
 *
 * @version    alpha
 *
 * @author     Bertrand Kintanar <bertrand@imakintanar.com>
 * @license    BSD License (3-clause)
 * @copyright  (c) 2017-2018, UPC Engineering, Inc.
 *
 * @link       https://gitlab.com/bkintanar/upcpi
 */

namespace UPCEngineering\Support\Permissions\Checkpoints;

class InactiveMember
{
    const STATUS_ACTIVE = 'active';
    const STATUS_INACTIVE = 'inactive';

    /**
     * @param string $permission
     * @param $access
     * @param null $member
     * @param null $user
     *
     * @return bool
     */
    public function handle(string $permission, array $access, $member = null, $user = null)
    {
        if (!is_null($member) && $member->status == self::STATUS_INACTIVE && str_contains($permission, ['.edit', '.update', '.store', '.destroy'])) {
            return false;
        }
    }
}
