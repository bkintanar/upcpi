<?php

/*
 * This file is part of the UPCPI Software package.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the 3-clause BSD License.
 *
 * This source file is subject to the 3-clause BSD License that is
 * bundled with this package in the LICENSE file.
 *
 * @version    alpha
 *
 * @author     Bertrand Kintanar <bertrand@imakintanar.com>
 * @license    BSD License (3-clause)
 * @copyright  (c) 2017-2018, UPC Engineering, Inc.
 *
 * @link       https://gitlab.com/bkintanar/upcpi
 */

namespace UPCEngineering\Support\Permissions;

use UPCEngineering\Support\Permissions\Checkpoints\HostPastor;
use UPCEngineering\Support\Permissions\Checkpoints\InactiveMember;
use UPCEngineering\Support\Permissions\Checkpoints\MemberSelfService;
use UPCEngineering\Support\Permissions\Checkpoints\SuperAdmin;

trait Permissible
{
    /**
     * @var array
     */
    protected $checkpoints = [
        // InactiveMember::class,
        SuperAdmin::class,
        HostPastor::class,
        MemberSelfService::class,
    ];

    /**
     * @param $permission
     * @param null $member
     *
     * @return bool
     *
     * @author Bertrand Kintanar <bertrand@imakintanar.com>
     */
    public function can($permission, $member = null): bool
    {
        $user = $this;

        $access = [
            'accessing-self'  => $member == $user->member,
            'can-access-self' => $user->hasMSSAccess(),
        ];

        foreach ($this->checkpoints as $checkpoint) {
            $result = (new $checkpoint())->handle($permission, $access, $member, $user);

            if (!is_null($result)) {
                return $result;
            }
        }

        return false;
    }

    /**
     * @return bool
     *
     * @author Bertrand Kintanar <bertrand@imakintanar.com>
     */
    protected function hasMSSAccess(): bool
    {
        $roles = $this->roles;

        return $roles->filter(function ($role) {
            return $role->hasPermissionTo('members.ess.index');
        })->count() ? true : false;
    }
}
